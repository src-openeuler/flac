Name: flac
Version: 1.5.0
Release: 1
Summary: encoder/decoder which support the Free Lossless Audio Codec
License: BSD-3-Clause AND GPL-2.0-or-later AND GFDL-1.1-or-later
Source0: https://ftp.osuosl.org/pub/xiph/releases/flac/flac-%{version}.tar.xz
URL: http://www.xiph.org/flac/

Patch0001:      flac-1.4.3-sw.patch

Provides: %{name}-libs = %{version}-%{release}
Obsoletes: %{name}-libs < %{version}-%{release}

BuildRequires:  gcc-c++ libogg-devel gcc automake autoconf libtool gettext-devel doxygen
BuildRequires:  desktop-file-utils

%description
FLAC stands for Free Lossless Audio Codec, an audio format similar to MP3,
but lossless, meaning that audio is compressed in FLAC without any loss in quality.

%package devel
Summary: FLAC libraries and header files for development.
Requires: %{name} = %{version}-%{release}

%description devel
FLAC libraries and header files for development.

%package_help

%prep
%autosetup -p1 -n %{name}-%{version}

%build
./autogen.sh -V

export CFLAGS="%{optflags} -funroll-loops"
%configure --disable-silent-rules --disable-thorough-tests

make

%install
%make_install
%delete_la

mv %{buildroot}%{_docdir}/flac* ./flac-doc
mkdir -p flac-doc-devel
mv flac-doc{/api,-devel}
rm flac-doc/FLAC.tag

%check
#make -C test check FLAC__TEST_LEVEL=0 &> /dev/null

%files
%doc flac-doc/* AUTHORS COPYING* README.md
%{_bindir}/flac
%{_bindir}/metaflac
%{_libdir}/*.so.*

%files devel
%{_includedir}/*
%{_libdir}/*.so
%{_libdir}/pkgconfig/*
%{_datadir}/aclocal/*.m4

%files help
%{_mandir}/man1/*
%doc flac-doc-devel/*

%changelog
* Fri Feb 21 2025 Funda Wang <fundawang@yeah.net> - 1.5.0-1
- update to 1.5.0

* Tue Dec 10 2024 shenzhongwei <shenzhongwei@kylinos.cn> - 1.4.3-4
- include all patches in the source package.

* Fri Aug 9 2024 zhangxingrong <zhangxingrong@uniontech.cn> - 1.4.3-3
- Limit the number of clock() calls
- Documentation (man/flac.md); fix typo 
- flac: foreign_metadata: fix -Walloc-size
- Fix format ending up with wrong subformat

* Fri Aug 9 2024 chenhaixiang<chenhaixiang3@huawei.com> - 1.4.3-2
- fix flac license

* Tue Feb 6 2024 chenhaixiang<chenhaixiang3@huawei.com> - 1.4.3-1
- update to 1.4.3

* Mon Jan 30 2023 chenhaixiang<chenhaixiang3@huawei.com> - 1.4.2-1
- update to 1.4.2

* Wed Nov 9 2022 chenhaixiang<chenhaixiang3@huawei.com> - 1.3.4-1
- update to 1.3.4

* Wed Oct 19 2022 wuzx<wuzx1226@qq.com> - 1.3.3-7
- add sw64 patch

* Sat May 28 2022 zhouwenpei <zhouwenpei1@h-partners.com> - 1.3.3-6
- fix CVE-2020-0499

* Wed Apr 27 2022 volcanodragon <linfeilong@huawei.com> - 1.3.3-5
- rebuild package

* Wed Apr 27 2022 volcanodragon <linfeilong@huawei.com> - 1.3.3-4
- disable check to avoid build error

* Sun Apr 24 2022 zhouwenpei <zhouwenpei1@h-partners.com> - 1.3.3-3
- enable makecheck

* Thu Mar 31 2022 zhouwenpei <zhouwenpei1@huawei.com> - 1.3.3-2
- fix CVE-2021-0561

* Fri Apr 24 2020 lihongjiang <lihongjiang6@huawei.com> - 1.3.3-1
- update version to 1.3.3

* Wed Jan 22 2020 lihongjiang <lihongjiang6@huawei.com> - 1.3.2-12
- ignore make check temporary

* Wed Jan 15 2020 openEuler Buildteam <buildteam@openeuler.org> - 1.3.2-11
- Fix build dependence

* Fri Sep 6 2019 lihongjiang <lihongjiang6@huawei.com> - 1.3.2-10
- Adding url into spec

* Fri Sep 6 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.3.2-9
- Package init
